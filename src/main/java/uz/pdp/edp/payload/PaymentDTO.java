package uz.pdp.edp.payload;


public record PaymentDTO(String orderId, Double amount) {
}

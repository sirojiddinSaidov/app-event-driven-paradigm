package uz.pdp.edp.payload;

public record OrderAddDTO(String name, String phone, Double amount) {
}

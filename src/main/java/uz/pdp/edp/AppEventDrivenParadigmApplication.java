package uz.pdp.edp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppEventDrivenParadigmApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppEventDrivenParadigmApplication.class, args);
    }

}

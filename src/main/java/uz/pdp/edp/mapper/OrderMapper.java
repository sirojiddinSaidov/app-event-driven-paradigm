package uz.pdp.edp.mapper;

import org.mapstruct.Mapper;
import uz.pdp.edp.collection.Order;
import uz.pdp.edp.payload.OrderAddDTO;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    Order toOrder(OrderAddDTO orderAddDTO);
}

package uz.pdp.edp.mapper;

import org.mapstruct.Mapper;
import uz.pdp.edp.collection.Payment;
import uz.pdp.edp.payload.PaymentDTO;

@Mapper(componentModel = "spring")
public interface PaymentMapper {

    Payment toPayment(PaymentDTO paymentDTO);
}

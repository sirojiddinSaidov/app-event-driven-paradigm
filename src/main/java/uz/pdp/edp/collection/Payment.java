package uz.pdp.edp.collection;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import uz.pdp.edp.enums.OrderStatusEnum;

import java.time.LocalDateTime;

@Document
@Data
public class Payment {

    @MongoId
    private ObjectId id;

    private String orderId;

    private LocalDateTime createdAt;

    private Double amount;
}

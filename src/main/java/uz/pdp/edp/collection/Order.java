package uz.pdp.edp.collection;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import uz.pdp.edp.enums.OrderStatusEnum;

import java.time.LocalDateTime;

@Document
@Data
public class Order {

    @MongoId
    private ObjectId id;

    private String name;

    private String phone;

    private LocalDateTime createdAt;

    private Double amount;

    private OrderStatusEnum status;
}

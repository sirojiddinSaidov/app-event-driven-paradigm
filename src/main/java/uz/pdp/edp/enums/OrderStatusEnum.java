package uz.pdp.edp.enums;

public enum OrderStatusEnum {

    PAYMENT_WAITING,
    PAID,
    COOKING,
    SENT,
    DELIVERED,
    CANCELLED,
    REJECTED
}

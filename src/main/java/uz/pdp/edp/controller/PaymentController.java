package uz.pdp.edp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.edp.payload.PaymentDTO;
import uz.pdp.edp.service.PaymentService;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class PaymentController {

    public static final String BASE_PATH = "/payment";
    private final PaymentService paymentService;

    @PostMapping
    public HttpEntity<Boolean> pay(@RequestBody PaymentDTO paymentDTO) {
        boolean res = paymentService.pay(paymentDTO);
        return ResponseEntity.ok(res);
    }
}

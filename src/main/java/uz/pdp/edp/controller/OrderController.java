package uz.pdp.edp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.edp.payload.OrderAddDTO;
import uz.pdp.edp.service.OrderService;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {


    private final OrderService orderService;

    @PostMapping("/create")
    public HttpEntity<String> add(@RequestBody OrderAddDTO orderAddDTO) {
        String paymentLink = orderService.create(orderAddDTO);
        return ResponseEntity.ok(paymentLink);
    }
}

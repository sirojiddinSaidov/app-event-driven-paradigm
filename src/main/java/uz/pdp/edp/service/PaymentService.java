package uz.pdp.edp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.edp.collection.Payment;
import uz.pdp.edp.mapper.PaymentMapper;
import uz.pdp.edp.payload.PaymentDTO;
import uz.pdp.edp.repository.PaymentRepository;

@Service
@RequiredArgsConstructor
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final PaymentMapper paymentMapper;
    private final OrderService orderService;


    public boolean pay(PaymentDTO paymentDTO) {
        Payment payment = paymentMapper.toPayment(paymentDTO);

        //todo check payment amount with order amount
        paymentRepository.save(payment);
        orderService.paid(payment.getOrderId());

        return true;
    }
}

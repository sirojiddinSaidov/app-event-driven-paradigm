package uz.pdp.edp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.edp.collection.Order;
import uz.pdp.edp.controller.PaymentController;
import uz.pdp.edp.enums.OrderStatusEnum;
import uz.pdp.edp.mapper.OrderMapper;
import uz.pdp.edp.payload.OrderAddDTO;
import uz.pdp.edp.repository.OrderRepository;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public String create(OrderAddDTO orderAddDTO) {
        Order order = orderMapper.toOrder(orderAddDTO);
        order.setStatus(OrderStatusEnum.PAYMENT_WAITING);
        orderRepository.save(order);
        return PaymentController.BASE_PATH + "?orderId=" + order.getId().toString();
    }

    public void paid(String orderId) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Order not found: " + orderId));

        order.setStatus(OrderStatusEnum.PAID);
        orderRepository.save(order);
    }
}

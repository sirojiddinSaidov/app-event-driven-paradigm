package uz.pdp.edp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.edp.collection.Payment;

@Repository
public interface PaymentRepository extends MongoRepository<Payment, String> {
}

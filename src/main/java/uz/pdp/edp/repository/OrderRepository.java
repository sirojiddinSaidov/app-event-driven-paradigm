package uz.pdp.edp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.edp.collection.Order;

@Repository
public interface OrderRepository extends MongoRepository<Order, String> {
}
